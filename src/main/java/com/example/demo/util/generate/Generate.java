package com.example.demo.util.generate;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class Generate {
    private Random random = new Random();
    private final Random RANDOM = new SecureRandom();
    private final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public int genDigits() {
        return 10000000 + random.nextInt(90000000);
    }

    public String getSalt(int length) {
        StringBuilder returnValue = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }
        return new String(returnValue);
    }

    public String convertStringDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        return strDate;
    }

    public String convertMilToString(long timeSTamp) {
        Date date = new Date(timeSTamp);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateFormatted = formatter.format(date);
        return dateFormatted;
    }

    public String convertIntStatusToString(int i) {
        String stt = "";
        if (i == -1) {
            stt = "Deleted";
        } else if (i == 0) {
            stt = "DeActive";
        } else if (i == 1) {
            stt = "Active";
        }
        return stt;
    }

    public String convertIntRoleToString(int i) {
        String role = "";
        if (i == 1) {
            role = "Member";
        } else if (i == 2) {
            role = "Admin";
        }
        return role;
    }

    public String convertIntGenderToString(int i){
        String gen = "";
        if (i == 0){
            gen = "Female";
        }else if (i == 1){
            gen = "Male";
        }else if (i == 2){
            gen = "Others";
        }
        return gen;
    }
}
