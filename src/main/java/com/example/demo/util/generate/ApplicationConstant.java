package com.example.demo.util.generate;

public class ApplicationConstant {
    public static final String LOGIN_WRONG_PASSWORD = "Something wrong";
    public static final String LOGIN_WRONG_EMAIL = "Email does not exist";
    public static final String DO_NOT_PERMISSION = "You do not have permission";
    public static final String EMAIL_EXIST = "Email has been exist";
    public static final String REGISTER_SUCCESS = "Register Success";
    public static final String REGISTER_FAIL = "Register Fail";
    public static final String LOGIN_PLEASE_ACTIVE = "Please active this account";
    public static final String LOGIN_BLOCK = "This account has been banned.";
    public static final String CURRENT_LOGGED_IN = "currentLoggedIn";
    public static final String NOT_FOUND = "Not Found";
    public static final String CHANGE_AVATAR_SUCCESS = "Change avatar Success";
    public static final String CHANGE_AVATAR_FAILED = "Change avatar Failed";
}
