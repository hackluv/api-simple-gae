package com.example.demo.service;

import com.example.demo.entity.MessageBlob;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class ImageService {
    private static BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
    MessageBlob messageBlob = new MessageBlob();

    public MessageBlob imageUrl(HttpServletRequest req, HttpServletResponse resp) {
        Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
        List<BlobKey> blobKeys = blobs.get("myFile");
        ImagesService imagesService = ImagesServiceFactory.getImagesService();
        if (blobKeys != null && blobKeys.size() > 0) {
            ServingUrlOptions servingUrlOptions = ServingUrlOptions.Builder.withBlobKey(blobKeys.get(0));
            String servingUrl = imagesService.getServingUrl(servingUrlOptions);
            messageBlob.setServingUrl(servingUrl);
            messageBlob.setBlobKey(blobKeys.get(0).getKeyString());
        }

        return messageBlob;
    }

    public String createUploadUrl() {
        String blobUploadUrl = blobstoreService.createUploadUrl("/api/v1/member/changeAvatar");
        return blobUploadUrl;
    }
}
