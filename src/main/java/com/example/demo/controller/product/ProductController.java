package com.example.demo.controller.product;

import com.example.demo.entity.Product;
import com.example.demo.model.ProductModel;
import com.google.gson.Gson;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class ProductController extends HttpServlet {
    static {
        ObjectifyService.register(Product.class);
    }

    ProductModel model = new ProductModel();
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setHeader("Cache-Control", "nocache");
        resp.setCharacterEncoding("utf-8");
        String id = req.getParameter("id");
        if (id!=null && id.length() > 0){
            Product product1 = model.getProductById(Long.parseLong(id));
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.getWriter().print(gson.toJson(product1));
        }else {
            List<Product> list = model.listProduct();
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.getWriter().print(gson.toJson(list));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setHeader("Cache-Control", "nocache");
        resp.setCharacterEncoding("utf-8");
        BufferedReader reader = req.getReader();
        Product product = gson.fromJson(reader, Product.class);
        if (checkExist(product) == true) {
            Product productSave = new Product();
            productSave.setTitle(product.getTitle());
            productSave.setImage(product.getImage());
            productSave.setPrice(product.getPrice());
            productSave.setStatus(Product.Status.ACTIVE);
            boolean check = model.addProduct(productSave);
            if (check == true) {
                resp.setStatus(HttpServletResponse.SC_CREATED);
                resp.getWriter().print(gson.toJson("Success"));
            } else {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().print(gson.toJson("Add game Failed"));
            }
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().print(gson.toJson("This game has been exist"));
        }

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setHeader("Cache-Control", "nocache");
        resp.setCharacterEncoding("utf-8");
        Product product = gson.fromJson(req.getReader(), Product.class);
        String id = req.getParameter("id");
        if (id!=null && id.length() > 0) {
            Product product1 = model.getProductById(Long.parseLong(id));
            product1.setPrice(product.getPrice());
            product1.setImage(product.getImage());
            product1.setTitle(product.getTitle());
            boolean check = model.updateProduct(product1);
            if (check == true){
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.getWriter().print(gson.toJson(product1));
            }else{
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().print(gson.toJson("Update Failed"));
            }
        }else {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            resp.getWriter().print(gson.toJson("Not Found"));
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setHeader("Cache-Control", "nocache");
        resp.setCharacterEncoding("utf-8");
        String id = req.getParameter("id");
        if (id!=null && id.length() > 0) {
            Product product1 = model.getProductById(Long.parseLong(id));
            product1.setStatus(Product.Status.DELETED);
            boolean check = model.deleteProduct(product1);
            if (check == true){
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.getWriter().print(gson.toJson("Delete Success"));
            }else {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().print(gson.toJson("Delete Failed"));
            }
        }else {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            resp.getWriter().print(gson.toJson("Not Found"));
        }
    }

    private boolean checkExist(Product product) {
        Product product1 = model.getProductByTitle(product.getTitle());
        if (product1 == null) {
            return true;
        } else {
            return false;
        }
    }
}
