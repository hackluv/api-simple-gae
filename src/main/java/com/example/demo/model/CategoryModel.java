package com.example.demo.model;

import com.example.demo.entity.Category;

import java.util.ArrayList;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CategoryModel {
    public boolean addCategory(Category category){
        ofy().save().entity(category).now();
        return true;
    }

    public ArrayList<Category> getListCategory(){
        ArrayList<Category> listCategory = (ArrayList<Category>) ofy().load().type(Category.class).list();
        return listCategory;
    }

}
