package com.example.demo.model;

import com.example.demo.entity.Product;

import java.util.ArrayList;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ProductModel {
    public boolean addProduct(Product product) {
        ofy().save().entity(product).now();
        return true;
    }

    public List<Product> listProduct() {
        List<Product> listProduct = ofy().load().type(Product.class).filter("status !=", -1).list();
        return listProduct;
    }

    public boolean updateProduct(Product product) {
        ofy().defer().save().entity(product);
        return true;
    }

    public boolean deleteProduct(Product product) {
        ofy().defer().save().entity(product);
        return true;
    }

    public Product getProductById(long id) {
        Product product = ofy().load().type(Product.class).id(id).now();
        return product;
    }

    public Product getProductByTitle(String title) {
        Product product = ofy().load().type(Product.class).filter("title", title).first().now();
        return product;
    }
}
